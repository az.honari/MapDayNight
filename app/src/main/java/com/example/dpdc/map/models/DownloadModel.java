package com.example.dpdc.map.models;

/**
 * Created by amirhossein on 11/17/2017.
 */

public class DownloadModel {
    String url ;
    int percent ;

    public DownloadModel(String url, int percent) {
        this.url = url;
        this.percent = percent;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
