
package com.example.dpdc.map.models;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YahooModel implements Serializable, Parcelable
{

    @SerializedName("query")
    @Expose
    private Query query;
    public final static Creator<YahooModel> CREATOR = new Creator<YahooModel>() {


        @SuppressWarnings({
            "unchecked"
        })
        public YahooModel createFromParcel(Parcel in) {
            return new YahooModel(in);
        }

        public YahooModel[] newArray(int size) {
            return (new YahooModel[size]);
        }

    }
    ;
    private final static long serialVersionUID = -1010372618269718126L;

    protected YahooModel(Parcel in) {
        this.query = ((Query) in.readValue((Query.class.getClassLoader())));
    }

    public YahooModel() {
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(query);
    }

    public int describeContents() {
        return  0;
    }

}
