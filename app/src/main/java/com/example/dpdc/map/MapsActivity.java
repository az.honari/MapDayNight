package com.example.dpdc.map;

import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.dpdc.map.models.Forecast;
import com.example.dpdc.map.models.YahooModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    final String temp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);







    }






    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        //String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date(1));
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("HH");
        String formattedDate = df.format(c.getTime());


        if (Integer.parseInt(formattedDate) > 17 ) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = mMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this, R.raw.uber));

                if (!success) {
                    Log.e("MapsActivityRaw", "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e("MapsActivityRaw", "Can't find style.", e);
            }
        }






        // Add a marker in Sydney and move the camera
        LatLng tehran = new LatLng(35.770505, 51.412748);
        mMap.addMarker(new MarkerOptions().position(tehran).title("tehran"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tehran));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                final String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                        "tehran" + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getWeather(url);
                    }
                }).start();

                return false;
            }

        });

        //--------------------

        //String city= new MarkerOptions().position(tehran).title("tehran").toString();








        // Add a marker in Sydney and move the camera
        LatLng bandarAbas = new LatLng(27.193436, 56.280695);
        mMap.addMarker(new MarkerOptions().position(bandarAbas).title("bandarAbas"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(bandarAbas));






    }








    //-----------------------------------------------

    public void getWeather(String url) {
        try {

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getShowTempByGson(response.toString());
                    }
                });


            }


        } catch (Exception e) {
            Log.d("", "getWeather: Exception:" + e);
        }


    }



    void getShowTempByGson(String serverResponse) {
        Gson gson = new Gson();
        YahooModel weather = gson.fromJson(serverResponse, YahooModel.class);

        for (Forecast forecat : weather.getQuery().getResults().getChannel().getItem().getForecast()) {

        }


        if (weather.getQuery().getCount() >= 1)
            Toast.makeText(this, weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "city wrong", Toast.LENGTH_SHORT).show();
    }


}
